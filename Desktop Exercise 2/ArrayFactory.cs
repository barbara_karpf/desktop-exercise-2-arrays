﻿using System;
using System.Linq;
using System.Collections;

namespace Desktop_Exercise_2
{
  public static class ArrayFactory
  {
    /// <summary>
    /// random number generator
    /// </summary>
    private static readonly Random random = new Random();

    /// <summary>
    /// returns an array of 10 random numbers
    /// </summary>
    /// <returns></returns>
    public static decimal[] GetArray(int arrLength, decimal minValue, decimal maxValue)
    {
      var arr = new decimal[arrLength];

      for (int i = 0; i < arr.Length; i++)
      {
        // see RandomNumberBetween defined at bottom
        arr[i] = RandomNumberBetween(minValue, maxValue);
      }
      // returns a decimal array
      return arr;
    }

    /// <summary>
    /// Output the contents of the array in the current order.
    /// </summary>
    public static void OutputArray(decimal[] arrayIn)
    {
      var arrayLength = arrayIn.Length;

      for (var i = 0; i < arrayIn.Length; i++)
      {
        // Note: this also works??  We were told to use String.Format.  Why?
        // Console.WriteLine("\nArray[{0}] = {1:C} ", i, arrayIn[i]);
        Console.WriteLine(String.Format("\nArray[{0}] = {1:C}", i, arrayIn[i]));
      }
    }

    /// <summary>
    /// Find the average value of the items in the array.
    /// </summary>
    public static void AverageArrayValue(decimal[] arrayIn)
    {
      var arrayTotal = (decimal)0;

      foreach (var item in arrayIn)
      {
        arrayTotal += item;
      }

      var arrayAvg = arrayTotal / arrayIn.Length;

      Console.WriteLine(String.Format("\nTotal of Items in Array = {0:C}", arrayTotal));
      Console.WriteLine(String.Format("\nArray Average found the HARD way is = {0:C}", arrayAvg));
      Console.WriteLine(String.Format("\nArray Average found the EASY way is = {0:C}", arrayIn.Average()));
    }

    /// <summary>
    /// Find the item with the lowest value in the array.
    /// </summary>
    public static void MinArrayValue(decimal[] arrayIn)
    {
      // set min to first item of array and replace if you find anything smaller
      var minArrayVal = arrayIn[0];
      var minArrayIndex = 0;
      var i = 1;
      do
       {
          if (minArrayVal > arrayIn[i]) {
            minArrayIndex = i;
            minArrayVal = arrayIn[i];
          };
          i++;
       } while (i < arrayIn.Length) ;

      Console.WriteLine(String.Format("\n Min Array Value found the HARD way is = {0:C}", minArrayVal));
      Console.WriteLine(String.Format("\t It was found in location: {0}", minArrayIndex));

      Console.WriteLine(String.Format("\n Min Array Value found the EASY way is = {0:C}", arrayIn.Min()));
      Console.WriteLine(String.Format("\t It was found in location: {0}", arrayIn.ToList().IndexOf(arrayIn.Min())));
    }

    /// <summary>
    /// Find the item with the higest value in the array.
    /// </summary>
    public static void MaxArrayValue(decimal[] arrayIn)
    {
      // set max to first item of array and replace if you find anything larger
      var maxArrayVal = arrayIn[0];
      var maxArrayIndex = 0;
      var i = 1;
      do
      {
        if (maxArrayVal < arrayIn[i])
        {
          maxArrayIndex = i;
          maxArrayVal = arrayIn[i];
        };
        i++;
      } while (i < arrayIn.Length);
    
      Console.WriteLine(String.Format("\n Max Array Value found the HARD way is = {0:C}", maxArrayVal));
      Console.WriteLine(String.Format("\t It was found in location: {0}", maxArrayIndex));

      Console.WriteLine(String.Format("\n Max Array Value found the EASY way is = {0:C}", arrayIn.Max()));
      Console.WriteLine(String.Format("\t It was found in location: {0}", arrayIn.ToList().IndexOf(arrayIn.Max())));
    }


    /// <summary>
    /// Sort the array so the contents are in ascending order.
    /// </summary>
    public static void SortArrayAsc(ref decimal[] arrayIn, bool easySort) {

      if (easySort)
      {
        Array.Sort(arrayIn);
      }
      else {
        // start at first location and look to see if any number in rest of
        // array is smaller
        // j steps through array and checks to make sure that location has the 
        // next smallest number
        // i loops through the rest of the array looking for smaller number than 
        // what is in arr[j]

        var numToSwap = (decimal)0;

        // make sure sorted smallest to largest by checking each location in array
        // and swapping numbers if smaller number found
        for (var j = 0; j < arrayIn.Length; j++)
        {

          // step through remaining positions in array looking for the next smallest number than
          // item currently at arr[j]; if detected, swap numbers
          for (var i = j + 1; i < arrayIn.Length; i++)
          {
            if (arrayIn[j] > arrayIn[i])
            {
              numToSwap = arrayIn[i];
              arrayIn[i] = arrayIn[j];
              arrayIn[j] = numToSwap;
            }

          }
        }
      }
    }


    /// <summary>
    /// generates a random decimal number between the given min and max
    /// </summary>
    /// <param name="min"></param>
    /// <param name="max"></param>
    /// <returns></returns>
    private static decimal RandomNumberBetween(decimal min, decimal max)
    {
      var next = (decimal)random.NextDouble();

      return min + (next * (max - min));
    }
  }
}
