﻿using System;
using System.Text.RegularExpressions;

namespace Desktop_Exercise_2
{
  class Program
  {
    static void Main(string[] args)
    {
      var numItemsInArray = new int();
      var numMinInArray = new decimal();
      var numMaxInArray = new decimal();
      string isItNumber;

      do
      {
        Console.WriteLine("How many prices do you want to put in your array?");
        Console.WriteLine("\tPlease enter an integer.\n");
        isItNumber = Console.ReadLine();

      // check for regular expression that is an integer 
      // @ allows reserved words
      // ^ is start of string, \d refers to only 0-9, + means one or more, $ is end of string 
      } while (!Regex.IsMatch(isItNumber, @"^\d+$"));

      // string from ReadLine is valid; must parse to an integer to use
      numItemsInArray = int.Parse(isItNumber);

      do
      {
        Console.WriteLine("\nWhat is the minimum price in your array?");
        Console.WriteLine("\tPlease enter a price without a $, but with cents."); 
        Console.WriteLine("\t(Example: 5.00 to represent $5.00.)\n");
        isItNumber = Console.ReadLine();

        // check for regular expression that is a currency entered without the $ 
        // @ allows reserved words, ^ is beginning of string, $ is end of string
        // ([0-9]\d{0,})+ : matches the dollar part, must have one or more digits 
        //                  one digit, followed by a second digit 0 or more times
        // (\.\d{2,2}): refers to cents; \. is escape for "." 
        //              and d{2.2} means exactly 2 digits after the "."
      } while (!Regex.IsMatch(isItNumber, @"^([0-9]\d{0,})+(\.\d{2,2})$"));

      // string from ReadLine is valid; must parse to a decimal to use
      numMinInArray = decimal.Parse(isItNumber);

      do
      {
        Console.WriteLine("\nWhat is the maximum value in your array?");
        Console.WriteLine("\tPlease enter a price without a $, but with cents.");

        Console.WriteLine("\t(Example: 5.00 to represent $5.00.)\n");
        isItNumber = Console.ReadLine();

      // regular expression to match currency input for maximum value; see above for explanation
      } while (!Regex.IsMatch(isItNumber, @"^([0-9]\d{0,})+(\.\d{2,2})$"));

      // string from ReadLine is valid; must parse to a decimal to use
      numMaxInArray = decimal.Parse(isItNumber);

      // set arr to output of ArrayFactory.GetArray method which is a decimal array
      var arr = ArrayFactory.GetArray(numItemsInArray, numMinInArray, numMaxInArray);

      // copy this array as we want to try to sort it the EASY way after doing it the HARD way
      // copying with var arrCopy = arr doesn't work as both point to same object and arrCopy
      // will be sorted already; must clone instead
  
      decimal[] arrClone = (decimal[])arr.Clone();
      
      Console.WriteLine("\n**** OutputArray ****");
      ArrayFactory.OutputArray(arr);
      
      Console.WriteLine("\r\n**** AverageArrayValue ****");
      ArrayFactory.AverageArrayValue(arr);

      Console.WriteLine("\r\n**** MinArrayValue ****");
      ArrayFactory.MinArrayValue(arr);

      Console.WriteLine("\r\n**** MaxArrayValue ****");
      ArrayFactory.MaxArrayValue(arr);

      Console.WriteLine("\r\n**** SortArrayAsc HARD ****");

      Console.WriteLine("\r\n Starting Array");
      ArrayFactory.OutputArray(arr);

      Console.WriteLine("\r\n**** Sorted the HARD Way ****");
      ArrayFactory.SortArrayAsc(ref arr, false);
      ArrayFactory.OutputArray(arr);

      // Want to sort the EASY way, but as we passed array by reference it has changed;
      // must pass in the copy of the original array that we made and sort it to compare to 
      // the HARD way method
      Console.WriteLine("\r\n**** SortArrayAsc EASY ****");
      Console.WriteLine("\r\n Starting Array");
      ArrayFactory.OutputArray(arrClone);

      Console.WriteLine("\r\n**** Done the EASY Way ****");
      ArrayFactory.SortArrayAsc(ref arrClone, true);
      ArrayFactory.OutputArray(arrClone);

      // Waiting for input; prevents console window from going away
      Console.Read();
    }
  }
}
